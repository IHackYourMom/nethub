import React, { Component } from 'react';

import ProfileTabs from './SubComponents/ProfileTabs/ProfileTabs'

class ProfileInfo extends Component {
  render() {
    return (
      <div className="row bg-light">
        <div className="col text-center">
          <div className="profile-info">
            <img
              src='http://cdn.arstechnica.net/wp-content/uploads/sites/3/2015/08/LinuxCon_Europe_Linus_Torvalds_05.jpg'
              alt="avatar"
            />
            <h4>Linus Torvalds</h4>
            <h5>@torvalds • Member since September 21, 2017</h5>
            <a href="mail#">linus@yandex.ru</a>
          </div>

          <ProfileTabs />

        </div>
      </div>
    )
  }
}

export default ProfileInfo;
