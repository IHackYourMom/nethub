import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'

import Overview from './SubComponents/Overview/Overview'
import LearningNets from './SubComponents/LearningNets'

class MainContent extends Component {
  render() {
    return(
      <div className="main-content">
        <Switch>
          <Route path='/overview' component={Overview} />
          <Route path='/learningnets' component={LearningNets} />
        </Switch>
      </div>
    )
  }
}

export default MainContent;
