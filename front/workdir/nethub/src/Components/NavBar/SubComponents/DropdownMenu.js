import React from "react";

export const DropdownMenu = (props) => {
  return(
    <div className="dropdown-menu dropdown-menu-right">
      <a className="dropdown-item" href="main#">Action</a>
      <a className="dropdown-item" href="main#">Another Action</a>
      <div className="dropdown-divider" href="main#"></div>
      <a className="dropdown-item" href="main#">Something else here</a>
    </div>
  );
}
