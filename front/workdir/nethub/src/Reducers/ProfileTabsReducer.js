const ProfileTabsReducer = (state ={
  overview: false,
  activity: false,
  groups: false,
  learning_nets: false
}, action) => {

  switch (action.type) {
    case "SET_OVERVIEW":
      state = {
        ...state,
        overview: action.payload
      }
      break;

    case "SET_LEARNING_NETS":
      state = {
        ...state,
        learning_nets: action.payload
      }
      break;

    default:
      break

  }
  return state
}

export default ProfileTabsReducer
