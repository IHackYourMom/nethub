import graphene
import graphql_relay
import django_filters
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from nets.models import  Net
from users.schema import UserType
# Filter classes

class NetFilter(django_filters.FilterSet):
    class Meta:
        model = Net
        fields = ['name']

# Node classes for relay queries

class NetNode(DjangoObjectType):
    class Meta:
        model = Net
        interfaces = (graphene.relay.Node,)

class NetType(DjangoObjectType):
    class Meta:
        model = Net


class CreateNet(graphene.relay.ClientIDMutation):
    id = graphene.Int()
    name = graphene.String()

    class Input:
        name = graphene.String()

    def mutate_and_get_payload(self, info, name):
        user = info.context.user or None
        if user.is_anonymous:
            raise Exception('Not logged in!')

        net = Net(
            name=name,
            user=user,
        )
        net.save()

        return CreateNet(
            id=net.id,
            name=net.name,
        )
# Mutations

class Mutation(graphene.ObjectType):
    create_net = CreateNet.Field()
# Query

class Query(object):
    net = graphene.relay.Node.Field(NetNode)
    nets = DjangoFilterConnectionField(NetNode, filterset_class= NetFilter)
