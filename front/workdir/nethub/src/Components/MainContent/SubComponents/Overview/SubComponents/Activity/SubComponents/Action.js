import React from "react";

export const Action = (props) => {
  //let action = props
  return(
    <div className="container action">
      <div className="row">

        <div className="col-md-6">
          <h6>Random action</h6>
        </div>

        <div className="col-md-6">
          <p>September 21, 2017</p>
        </div>

      </div>
      <hr />
    </div>
  )
}
