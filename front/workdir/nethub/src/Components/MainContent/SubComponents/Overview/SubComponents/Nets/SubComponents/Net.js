import React from "react";

export const Net = (props) => {
  let net = props
  return(
    <div className="container net">
      <div className="row">
        <div className="col-md-6">
          <a href="name#">
            <h6>Linus Torvalds / {net.name}</h6>
          </a>
        </div>

        <div className="col-md-6 update">
          <div className="row">
            <p className="text-right">Last update: today</p>
          </div>
        </div>

      </div>
      <hr />
    </div>
  )
}
