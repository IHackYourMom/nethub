from django.conf import settings
from django.db import models

class Net(models.Model):
    name = models.CharField(max_length=100)
    epoch = models.IntegerField(null=True)
    inputs = models.IntegerField(null=True)
    outputs = models.IntegerField(null=True)
    hidden_layer = models.IntegerField(null=True)
    path_to_git = models.CharField(max_length=100, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,null=True,
                             on_delete=models.CASCADE)
