from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import subprocess
from flask_cors import CORS
import os
app = Flask(__name__)
api = Api(app)
CORS(app)

def add_user(user_name):
    newpath = '/workdir/users/'+user_name
    if not os.path.exists(newpath):
        os.makedirs(newpath)
def del_user(user_name):
    delpath = '/workdir/users/'+user_name
    os.rmdir(delpath)
def add_repo(user_name, repo_name):
    newpath = '/workdir/users/'+user_name+'/'+repo_name
    if not os.path.exists(newpath):
        os.makedirs(newpath)
def del_repo(user_name, repo_name):
    delpath = '/workdir/users/'+user_name+'/'+repo_name
    os.rmdir(delpath)

class AddUser(Resource):
    def get(self, user_name):
        add_user(user_name)
class DelUser(Resource):
    def get(self, user_name):
        del_user(user_name)
class AddRepo(Resource):
    def get(self, user_name, repo_name):
        add_repo(user_name, repo_name)
class DelRepo(Resource):
    def get(self, user_name, repo_name):
        del_repo(user_name, repo_name)

api.add_resource(AddUser, '/add_user/<user_name>')
api.add_resource(DelUser, '/del_user/<user_name>')
api.add_resource(AddRepo, '/add_repo/<user_name>/<repo_name>')
api.add_resource(DelRepo, '/del_repo/<user_name>/<repo_name>')

if __name__ == '__main__':
    app.run(debug=True)
