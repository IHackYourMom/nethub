import { createStore, combineReducers, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'

/* Reducers */
import ProfileTabsReducer from './Reducers/ProfileTabsReducer'

export default createStore(
  combineReducers({
    ProfileTabsReducer
  }),
  {},
  applyMiddleware(createLogger())
)
