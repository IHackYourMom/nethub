import React, { Component } from 'react';

/* SubComponents */
import { Action } from './SubComponents/Action'

/* Styles */
import './Styles/Actions.css'

class Activity extends Component {
  render() {
    return(
      <div>
        <h4 className="mb-5">Activity</h4>

        <ul className="list-group">
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
          <Action />
        </ul>

      </div>
    )
  }
}

export default Activity;
