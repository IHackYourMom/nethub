import React, { Component } from 'react';

/* SubComponents */
import { NavBarSearch } from './SubComponents/NavBarSearch'
import { NavBarContent } from './SubComponents/NavBarContent'

class NavBar extends Component {
  render() {
    return (
      <div>

        <nav className="navbar navbar-expand navbar-dark bg-dark static-top">

          <a className="navbar-brand mr-1" href="main#">NetHub</a>

          <NavBarSearch />
          <NavBarContent />

        </nav>

      </div>
    );
  }
}

export default NavBar;
