import React, { Component } from 'react';

/* Components */
import ProfileInfo from '../Components/ProfileInfo/ProfileInfo'
import MainContent from '../Components/MainContent/MainContent'

/* Styles */
import './Styles/Content.css'

class Content extends Component {
  render() {
    return(
      <div className="container-fluid">
        <div className="container">

          <ProfileInfo />
          <MainContent />

        </div>
      </div>
    );
  }
}

export default Content;
