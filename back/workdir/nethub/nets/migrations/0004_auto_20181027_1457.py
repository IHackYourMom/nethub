# Generated by Django 2.0.8 on 2018-10-27 14:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nets', '0003_vote'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vote',
            name='net',
        ),
        migrations.RemoveField(
            model_name='vote',
            name='user',
        ),
        migrations.DeleteModel(
            name='Vote',
        ),
    ]
