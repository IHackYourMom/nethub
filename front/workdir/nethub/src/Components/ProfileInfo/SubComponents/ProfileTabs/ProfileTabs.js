import React, { Component } from 'react';

/* SubComponents */
import { Tab } from './SubComponents/Tab'

class ProfileTabs extends Component {
  render() {
    return (
      <div>
        <ul className="nav nav-tabs">

          <Tab name="Overview" isActive={false} />
          <Tab name="Activity" isActive={false} />
          <Tab name="Groups" isActive={false} />
          <Tab name="Learning nets" isActive={false} />

        </ul>
      </div>
    )
  }
}

export default ProfileTabs;
