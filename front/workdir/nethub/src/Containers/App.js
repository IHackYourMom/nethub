import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'

/* Components */
import NavBar from '../Components/NavBar/NavBar'
import Content from './Content'
import Login from './Login'

import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route exact path="/overview" component={Content} />
          <Route exact path="/" component={Login} />
        </Switch>

      </div>
    );
  }
}

export default App;
