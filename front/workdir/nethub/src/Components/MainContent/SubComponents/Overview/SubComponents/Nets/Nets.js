import React, { Component } from 'react';

/* SubComponents */
import { Net } from './SubComponents/Net'

/* Styles */
import './Styles/Nets.css'

class Nets extends Component {
  render() {
    return (
      <div>
        <h4 className="mb-5">Nets</h4>

        <ul className="list-group">
          <Net name="My 4 net" />
          <Net name="My third net" />
          <Net name="My second net" />
          <Net name="My first net" />
        </ul>
      </div>
    )
  }
}

export default Nets;
