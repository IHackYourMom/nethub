import React from "react";

/* SubComponents */
import { DropdownMenu } from './DropdownMenu'

export const NavBarContent = (props) => {
  return(
    <ul className="navbar-nav ml-auto ml-md-0">
      <li className="nav-link dropdown no-arrow mx-1">
        <a className="nav-link dropdown-toggle" href="main#" data-toggle="dropdown">
          <i className="fas fa-bell fa-fw"></i>
          <span className="badge badge-danger">9+</span>
        </a>
        <DropdownMenu />
      </li>
    </ul>
  );
}
