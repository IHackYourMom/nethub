import React, { Component } from 'react';

/* SubComponents */
import Activity from './SubComponents/Activity/Activity'
import Nets from './SubComponents/Nets/Nets'

class Overview extends Component {
  render() {
    return(
      <div className="row">

        <div className="col-md-6">
          <Activity />
        </div>

        <div className="col-md-6">
          <Nets />
        </div>

      </div>
    )
  }
}

export default Overview;
