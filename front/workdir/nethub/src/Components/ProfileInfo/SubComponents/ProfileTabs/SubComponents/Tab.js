import React from 'react';

export const Tab = (props) => {

  let tab = props
  let path = "/"+tab.name

  return (
    <li className="nav-item">
      {/*<Link to='/'>*/}
        {
          tab.isActive ?
          <a className="nav-link active" href={path.toLowerCase().replace(/\s/g,'')}>{tab.name}</a> :
          <a className="nav-link" href={path.toLowerCase().replace(/\s/g,'')}>{tab.name}</a>
        }
      {/*</Link>*/}
    </li>
  )
}
