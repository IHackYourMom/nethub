import React, { Component } from 'react'
import { GC_AUTH_TOKEN } from '../Constants'

/* Mutation */
import CreateUserMutation from '../Mutations/CreateUserMutation'
import TokenAuthMutation from '../Mutations/TokenAuthMutation'

/* Styles */
import './Styles/Login.css'

class Login extends Component {
  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    username: '',
  }

  render() {
    const { login, email, password, username } = this.state
    return (
      <div className="container login">
        <div className="row bg-light">

          <div className="col text-center">
            <h4>{login ? 'Login' : 'Sign Up'}</h4>

            <div className="form-group">
                <input
                value={username}
                onChange={e => this.setState({ username: e.target.value })}
                type="text"
                placeholder="Your name"
                className="form-control"
              />
            </div>

            <div className="form-group">
              {!login && (
                <input
                  value={email}
                  onChange={e => this.setState({ email: e.target.value })}
                  type="text"
                  placeholder="Your email address"
                  className="form-control"
                />
              )}
            </div>

            <div className="form-group">
              <input
                value={password}
                onChange={e => this.setState({ password: e.target.value })}
                type="password"
                placeholder="Choose a safe password"
                className="form-control"
              />
            </div>

            <hr />

            <div className="form-group row">
              <div className="col-sm-1">
                {login ? (
                  <button
                    className="btn btn-primary"
                    onClick={() => this._signInUser()}
                  >
                  Login
                  </button>
                ) : (
                  <button
                    className="btn btn-primary"
                    onClick={() => this._createUser()}
                  >
                  Create account
                  </button>
                )}

                <br />
                <br />

                {login ? (
                  <button
                    className="btn btn-secondary"
                    onClick={() => this.setState({ login: !login })}
                  >
                  Need to create an account?
                  </button>
                ) : (
                  <button
                    className="btn btn-secondary"
                    onClick={() => this.setState({ login: !login })}
                  >
                  Already have an account?
                  </button>
                )}

                <br />
                <br />
              </div>
            </div>

          </div>

        </div>
      </div>
    )
  }

  _createUser = () => {
    const { username, email, password } = this.state
    CreateUserMutation(username, email, password, () => console.log('Mutation complete'))
  }

  _signInUser = () => {
    const { username, password } = this.state
    TokenAuthMutation(username, password, (token) =>
    this._saveUserData(token))
  }

  _saveUserData = (token) => {
    localStorage.setItem(GC_AUTH_TOKEN, token)
  }



}

export default Login
