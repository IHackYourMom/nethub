export function setOverview(state) {
  return {
    type: "SET_OVERVIEW",
    payload: state
  }
}

export function setLearningNets(state) {
  return {
    type: "SET_LEARNING_NETS",
    payload: state
  }
}
